# Some prepared vagrant files for RHEL cert and learning stands

## How to generate SSH keys

```bash
ssh-keygen -t rsa -b 4096 -C vagrantboxes -f ~/.ssh/vagrant -N ""
```